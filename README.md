# Go Oauth2 Helper Module

[![GoDoc](https://godoc.org/gitlab.com/rwxrob/auth-go?status.svg)](https://godoc.org/gitlab.com/rwxrob/auth-go)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/rwxrob/auth-go)](https://goreportcard.com/report/gitlab.com/rwxrob/auth-go)
[![Coverage](https://gocover.io/_badge/gitlab.com/rwxrob/auth-go)](https://gocover.io/gitlab.com/rwxrob/auth-go)

Designed to help make command line Oauth2 easier to implement.

Always tweaking on it, but it works.

## TODO

* `StartTempServer(port)`
* Better host os detection for opening local graphic browser
* Allow default local server templates to be customized in `OAUTHDIR`
